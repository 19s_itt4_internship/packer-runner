#!/usr/bin/env bash

DL_DIR="download"
DL_FILENAME="VMware-VIX-1.15.8-5528349.x86_64.bundle"
BASEDIR=$(dirname $0)
cd $BASEDIR

echo " ** download the right vix version from vmware.com"
cd $DL_DIR
wget -N https://download3.vmware.com/software/player/file/$DL_FILENAME
cd ..

echo " ** - and run it"
sudo sh $DL_DIR/$DL_FILENAME --eulas-agree --required
