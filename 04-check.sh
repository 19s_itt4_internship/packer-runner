#!/usr/bin/env bash

DL_DIR="download"
BASEDIR=$(dirname $0)
cd $BASEDIR

echo " ** download check script"
mkdir -p $DL_DIR
cd $DL_DIR
wget -Nc https://raw.githubusercontent.com/moozer/ansible-server-kvm/master/files/kvm_sanity_check.sh
cd ..

echo " ** Run checks"
bash $DL_DIR/kvm_sanity_check.sh

echo " ** if the last one above is 'bad', consider a reboot"
