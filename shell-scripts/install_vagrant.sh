#!/usr/bin/env bash

curl -O https://releases.hashicorp.com/vagrant/2.2.3/vagrant_2.2.3_x86_64.deb
sudo apt install ./vagrant_2.2.3_x86_64.deb
cd ~/Downloads
wget -v http://cache.ruby-lang.org/pub/ruby/2.1/ruby-2.1.1.tar.gz
mkdir ~/applications
mv ~/Downloads/ruby-2.1.1.tar.gz ~/applications
cd ~/applications
tar -zxvf ruby-2.1.1.tar.gz
cd ruby-2.1.1
./configure
make
sudo make install


