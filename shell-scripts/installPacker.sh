#!/usr/bin/env bash

sudo apt-get update
sudo wget https://releases.hashicorp.com/packer/1.3.5/packer_1.3.5_linux_amd64.zip
sudo unzip packer_1.3.5_linux_amd64.zip -d /usr/local/packer
cd ~/
export PATH="$PATH:/user/local/packer"
sudo ln -s /usr/local/packer/packer packer
source ~/.bashrc