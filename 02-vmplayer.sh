#!/usr/bin/env bash

# fail script if anything fails
set -e

DL_DIR="download"
BASEDIR=$(dirname $0)
cd $BASEDIR

echo " ** installing packages"
apt-get install -y build-essential
apt-get install -y linux-headers-$(uname -r)
apt-get install -y open-vm-tools

echo " ** - installing extra libraries as suggested by the installer"
apt-get install -y libpcsclite1 libaio1

echo " ** - not installing open-vm-tools-desktop, should I?"
echo " ** - wanted to install vmware-user-wrapper but it doesn't exist"

echo " ** downloading player installer"
mkdir -p $DL_DIR
cd $DL_DIR
wget -N -nv https://www.vmware.com/go/getplayer-linux
chmod +x getplayer-linux
cd ..

echo " ** running player installation"
sh $DL_DIR/getplayer-linux --eulas-agreed --required
