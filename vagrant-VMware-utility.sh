#!/usr/bin/env bash

# fail script if anything fails

set -e

echo " ** The Vagrant VMware Utility is provided as a system package."  

wget https://www.vagrantup.com/vmware/downloads.html
sudo mkdir /opt/vagrant-vmware-desktop/bin 
sudo unzip -d /opt/vagrant-vmware-desktop/bin vagrant-vmware-utility_1.0.0_x86_64.zip
sudo /opt/vagrant-vmware-desktop/bin/vagrant-vmware-utility certificate generate
sudo /usr/local/vagrant-vmware-desktop/vagrant-vmware-utility service install
