#!/usr/bin /env bash

# fail script if anything fails
set -e

BASEDIR=$(dirname $0)
cd $BASEDIR

curl -k -O https://10.217.19.225:8081/WebShare/Debian-9.x.test.zip
unzip  Debian-9.x.test.zip
cp -r  Debian-9.x.test $HOME/vmware

