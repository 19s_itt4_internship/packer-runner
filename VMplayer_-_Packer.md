# Clone a Debian machine using VMplayer and Packer 

# Introdaction 

The main reason for this document is to provide a reflection of my research on Packer and VMplayer. The goal of this task is to clone a virtual machine,  that located in another virtual machine by using a Packer script.

# Configure a Debian 9 OS on ESXi.

## install Sudo

After the Debian installation, I installed**sudo** to allow unroot user to execute root commands. To do that login as root and execute the comand:
* **apt-get install sudo**

When sudo has been installed go to **/etc/sudoers/README** and remove the first hashtag from the fifth line. Than get in sudoers file with **nano /etc/sudoers** and type the users name under **User alias specification** like this:  **michail ALL=(ALL:ALL) ALL**. Now it is time to add the unroot user in sudo family, to do that execute the command: **adduser username sudo**.

[For more info follow the link)](https://medium.com/platform-engineer/how-to-enable-sudo-on-a-user-account-on-debian-494d3c75ee21)

## Install ssh

SSH stands for Secure Shell and provides a safe and secure way of executing commands, making changes, and configuring services remotely. When you connect through SSH, you log in using an account that exists on the remote server. 

To install ssh execute the command: 
* **sudo apt-get install ssh or openssh server**

[For more info follow the link)](https://wiki.debian.org/SSH)


## install Packer

Packer is an open source tool for creating identical machine images for multiple platforms from a single source configuration. Packer is lightweight, runs on every major operating system, and is highly performant, creating machine images for multiple platforms in parallel. Packer does not replace configuration management like Chef or Puppet. In fact, when building images, Packer is able to use tools like Chef or Puppet to install software onto the image.

To install Packer execute the command: 
* **sudo apt-get install packer**.
To set a variable PATH, go to 
* **nano ~/.profile**

in the end of the file import the following command:
* **export PATH=$PATH:~/packer/**.

[For more info follow the links](https://www.packer.io/intro/why.html) -
[Packer instalation](https://linoxide.com/linux-how-to/install-packer-creating-identical-machine-images/)

# Install VMplayer on Debian 9

To install vmplayer you must have installed the vmware tools, to do that start with the command: 
* **sudo apt-get install build-essentials**. 

The build-essentials package is a reference for all the packages needed to compile a Debian package. It generally includes the GCC/g++ compilers and libraries and some other utilities.

[For more info follow the link](https://packages.debian.org/sid/build-essential)

## Install linux-headers

The header files define an interface: they specify how the functions in the source file are defined.Kernel-headers are used to compile various kernel modules, such as the graphics card driver you are trying to install. Like other header files in source code, kernel headers declare various functions, variables and data structures.  

To install linux-headers in Debian execute the commands: 

* **sudo apt-get update**
* **sudo apt-get install linux-headers-$(uname -r)** 

Now, check if the matching kernel headers have been installed on your system using the following command: 
* **ls -l /usr/src/linux-headers-$(uname -r)** 

[for more info follow the links](https://askubuntu.com/questions/598948/what-does-linux-headers-uname-r-do)

## Download and install VMplayer

VMware is a virtualization and cloud computing software provider based in Palo Alto. VMware is a subsidiary of Dell Technologies.
To download the latest version of vmplayer from the commant line use the command: **wget https://www.vmware.com/go/getplayer-linux**.
To make the installation file executable type **sudo chmod +x getplayer-linux**.
Now it is time to install vmplayer, type the command **sudo ./getplayer-linux** and follow the installation procedure.

To enable virtualization on vmplayer, click the virtual machine that is off, and select **virtual machine settings**, and then **processors**. Under Virtualization Engine select all the choices.

## Install VMware-tools
 
 VMware Tools improves the performance and management of the virtual machine. In order to achieve full functionality in a VMware based virtual machine, VMware Tools are strongly recommended. 
 
 To install vmware tools follow the commands:

 * **sudo apt-get install open-vm-tools open-vm-tools-desktop**
 * **vmware-user-wrapper**

 To start the installation type:
 * **sudo sh ./file_name**

## Install vmware VIX API

The VIX API helps you write scripts to automate virtual machine operations and manipulate files within guest operating systems. VIX programs run on different systems and support management of vSphere, Workstation, Player, and Fusion.

To install vmware VIX API, download the package from this link [vmware VIX API](https://my.vmware.com/web/vmware/details?downloadGroup=PLAYER-1400-VIX1170&productId=734) and execute the command: 

* **sudo sh ./ file_name**

To uninstall the package type:
* **sudo vmware-installer -u vmware-vix**

## Unexpected Error

After the installation, I executed a Packer file that was validated from Packer, to check if it is working. Unfortunately, Packer couldn't recognise the VMplayer version and it is telling me to update it to version 6, while I have installed the latest version 15. Therefore I have made the same task on my laptop using VMware workstation pro with the same Packer.json file, and it worked. Then I compared my laptop's configuration with the VM's and it was the same.
I found several links with the same problem that attribute the error on the VMplayer license.

[packer: Error when building from source ova](https://stackoverflow.com/questions/51038349/packer-error-when-building-from-source-ova)

[vmware-vmx does not detect vmplayer version and refuses to clone](https://github.com/hashicorp/packer/issues/972)

['vmware-vmx' builder throws 'Cloning is not supported...' error](https://github.com/hashicorp/packer/issues/2395)
