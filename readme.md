packer-runner
===================

This repository contains scripts to install vmplayer, packer and related software on a debian-9 box.

## Overview 

Packer-runner repository has been created to automate images using shell scripts.

![packer-runner-diagram](Packer-runner-diagram.png)

This diagram illustrates the path from the packer-runner repository to the physical machine (clone Gitlab repository), creates the VM and install all the necessary software in the VM.

Packer-runner repository includes installation scrips for:
* The basic installation (updates) that the system needs to run the scripts.
* VMplayer that allows you to run a second, isolated operating system on a single PC.
* Vmware-vix is an API that lets users write scripts and programs to manipulate virtual machines.
* A zip vmx file that will be unzipped and copied the content of the files into VMware directory
* Packer that is  an open source tool for creating identical machine images for multiple platforms from a single source configuration.

They are also many other installation scripts like VirtualBox, KVM-libvirt, and vagrant. In order to clone packer-runner repository in your machine you should first install vagrant and libvirt, or you can run the shell scripts that I've made for you.

 [vagrant-libvirt](https://gitlab.com/19s_itt4_internship/packer-runner/tree/master/shell-scripts) 
