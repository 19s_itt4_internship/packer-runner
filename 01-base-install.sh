#!/usr/bin/env bash

# fail script if anything fails
set -e

BASEDIR=$(dirname $0)
cd $BASEDIR

echo " ** installing base packages"
apt-get update
apt-get install -y sudo
apt-get install -y ssh

echo " ** Note: Remember to add user to sudoers"

echo " ** enabling nesting (remember to reboot if applicable"
cp kvm-nested.conf /etc/modprobe.d
