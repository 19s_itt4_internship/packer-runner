#!/usr/bin/env bash

CURUSER=$(whoami)

#istall kvm
sudo apt-get install qemu-kvm libvirt-clients libvirt-daemon-system bridge-utils libguestfs-tools genisoimage virtinst libosinfo-bin
#create an iso image
cd /var/lib/libvirt/images/
sudo wget -c https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-9.8.0-amd64-netinst.iso
#add to groups
sudo adduser $CURUSER libvirt
sudo adduser $CURUSER libvirt-qemu
# networing
sudo virsh net-start default
# connect to KVM server
virsh --connect qemu:///system list --all
